Plaza Garland tiene una gran variedad de tiendas y restaurants. Un ambiente familiar, seguro y limpio. Ven a visitarnos donde encontraras ropa, accesorios, herramientas, salones de belleza y no te olvides de los tipicos sabores de nuestra tierra. Todo bajo un mismo techo.

Address: 3161 Broadway Blvd, Garland, TX 75043, USA

Phone: 469-562-4939

Website: https://www.plazagarland.com
